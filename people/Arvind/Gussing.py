# IIITMK AI course Gym Toy Text Guesing Game V0 Solution

# First we import necessery modules such as gym and numpy
import gym
import numpy as np

no_of_attempts = 200

# initialize the environment
envn = gym.make("GuessingGame-v0")
envn.reset()

# set the initial parameters
x = 1000
y = -1000

for attempts in range(no_of_attempts):
    print("Attempts:", attempts)

    # Choosing the value between the ranges
    guess = (x + y) / 2

    observation, reward, done, info = envn.step(np.array([guess]))

    # Chaing parameters depending on if the guess is higher or lower
    if observation == 1:
        print(guess, " is lower than the target")
        y = guess
    if observation == 2:
        print("Correct Value reached:", guess)
        break
    if observation == 3:
        print(guess, " is higher than the target")
        x = guess

envn.close()
