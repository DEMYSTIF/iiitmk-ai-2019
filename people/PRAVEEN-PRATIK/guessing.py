import gym
import numpy as np


env = gym.make("GuessingGame-v0")
env.reset()

guess = 0
lower = -1000
upper = 1000


def observation(guess):
    obs, reward, done, info = env.step(np.array([guess]))
    return obs


for i in range(1, 201):
    print("Step {}".format(i))
    obsr = observation(guess)
    if obsr == 1:
        print("{} is lower than target".format(guess))
        lower = guess
    if obsr == 3:
        print("{} is higher than target".format(guess))
        upper = guess
    if obsr == 2:
        print("{} is the number".format(guess))
        break

    guess = (lower + upper) / 2

env.close()
